<?php 
	include ('admin_header.php');
	session_start();
 

	// Check if the user is logged in, if not then redirect to login page
	if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
	    ?>
	    	<script type="text/javascript">
	    		window.open("../../admin/index.php")
	    	</script>
	   <?php 
	}

 ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<main class="container">
		<h1 style="color:white;">Administrator</h1>
		<div>
			<a href="user.php" style="color:yellow;">User</a>
		</div>
		
		<div>
			<a href="course.php" style="color:yellow;">Courses</a>
		</div>

		<a href="logout.php">Logout</a>
		
	</main>

</body>
</html>