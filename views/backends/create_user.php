<?php 
	include ('admin_header.php');
 ?>

 <!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		label{
			color:white;
		}
	</style>
</head>
<body>
	<main class="container">
		<h1 style="color:white;">Create User</h1>
		<div>
			<form action="process_create_user.php" method="POST">
			  <div class="form-group">
			    <label for="exampleInputEmail1">User Name</label>
			    <input type="text" class="form-control" id="username" name="username" placeholder="User Name" required="">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1">Password</label>
			    <input type="password" class="form-control" id="txt_password" name="txt_password" placeholder="Password" required="">
			  </div>

			  <div class="form-group">
			    <label for="exampleInputPassword1">Confirm password</label>
			    <input type="password" class="form-control" id="txt_confirm_password" name="txt_confirm_password" placeholder="Confirm password" required="">
			  </div>
			  
			
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
	</main>

</body>
</html>