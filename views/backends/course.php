<?php 
	session_start();
 
	// Check if the user is logged in, if not then redirect to login page
	if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
	    ?>
	    	<script type="text/javascript">
	    		window.open("../../admin/index.php")
	    	</script>
	   <?php 
	}

	include ('admin_header.php');
	// Include config file
	include('../../connection.php');
	$conn = Conn();
 ?>

 <!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		td{
			color:white;
		}
		tr{
			color:white;
		}
		a{
			color:yellow;
			text-decoration: none;
		}
		a:hover{
			text-decoration: none !important;
			color:red;
		}
		/*body{
			color:white;
		}*/
	


	</style>
</head>
<body>
	<?php
		

		$all_course = "SELECT * from tbl_course";
		$num = $conn->query($all_course);
		$num = $num->num_rows;
	
		$pagination = 4;
		$count_num_page_even =  floor($num / $pagination);

		

		$count_num_page_odd = $num % $pagination;

		$my_num = 0;
		if($count_num_page_odd!=0)
		{
			$my_num = 1;
		}

		$final_page = $count_num_page_even + $my_num;


		
		$sql = "SELECT * from tbl_course LIMIT $pagination";
		$r_result = $conn->query($sql);
	   
	?>
	<main class="container">
		<h1 style="color:white;">All course</h1>

		<div style="margin-bottom: 10px;">
			<a href="add_course.php" type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add course</a>
		</div>

		<div style="margin-bottom: 10px;">
			<input type="text" name="txt_search_title" id="txt_search_title">
			<button onclick="searchCourse()">Search</button>
		</div>
		<div>
			<table class="table table-bordered" id="tbl_course">
				<thead>
					<tr>
						<th>ID</th>
						<th>Title</th>
						<th>Price</th>
						<th>Image</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						 if ($r_result->num_rows > 0) {
							while($row = $r_result->fetch_assoc()) { 
							?>
								<tr>
									<td>
										<span><?php echo $row['id'] ?></span>
									</td>
									<td><?php echo $row['title']; ?></td>
									<td><?php echo $row['price']; ?></td>
									<td><img src="../../media/<?php echo $row['course_image']; ?>" width=80; height=80; class="img-rounded"/></td>
									<td style="color:red;">
										<a href="edit_course.php?pk=<?php echo $row['id'] ?>"> <span class="glyphicon glyphicon-pencil"></span> Edit </a> |
										<a href="delete_course.php?pk=<?php echo $row['id'] ?>"> <span class="glyphicon glyphicon-trash"></span> Delete </a>
									</td>
								</tr>
							<?php			 
							}
						}

					?>
					
				</tbody>
			</table>

			<div>
				<button> << back </button>
				<?php 
					for ($i=1; $i <= $final_page ; $i++) { 
						?>
							<button><?php echo $i; ?></button>
						<?php
					}
				?>
				<button> >> next</button>
			</div>
		</div>



		<script type="text/javascript">
			function searchCourse()
			{
				var txt_title = document.getElementById("txt_search_title").value;
				$.ajax({
			        url: "search_course.php",
			        type: "post",
			        data:  {
			        	'txt_title': txt_title
			        },
			        success: function (response) {
			        	// console.log(response);

			        	document.getElementById("tbl_course").innerHTML = response;
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			           console.log(textStatus, errorThrown);
			        }
			    });
			}
			
		</script>
	</main>

</body>
</html>