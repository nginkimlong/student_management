<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">


	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

	<!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

	 -->
	<link href="https://fonts.googleapis.com/css2?family=Moul&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Preahvihear&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/student_management/static/css/style.css">
	<link rel="stylesheet" type="text/css" href="/student_management/static/css/detail.css">
	

	
</head>
<body>
	<main>
		<header class="navbar navbar-static-top bs-docs-nav" id="top">
			<div class="container">
				<div class="navbar-header" style="text-align: center; width: 100%; margin-bottom: 30px;">
					<div class="row">
						<div class="col-md-12">
							
								<div>
									<nav id="bs-navbar">
										<a href="/student_management/index.php">
											<img src="/student_management/media/pic1.png" alt="logo" width="120px" height="120"> 
										</a>
									</nav>
								</div>
								<div>
							
									<nav id="bs-navbar">
										<ul class="nav" style="color:white;">
											<h3 style="font-family: 'Moul">មជ្ឈមណ្ឌលស្រាវជ្រាវបច្ចេកវិទ្យាហ្គ្រេដ</h3>
											<p>GreatX Center</p>
										</ul>
									</nav>
								</div>
							
						</div>
					</div>
				</div>
					<!-- <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-navbar" aria-controls="bs-navbar" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button> -->
					<!-- <a href="/docs/3.4/" class="navbar-brand">Bootstrap</a> -->
					
				<!-- <nav id="bs-navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav" style="color:white;">
					<h2>មជ្ឍមណ្ឌលស្រាវជ្រាវបច្ចេកវិទ្យាហ្គ្រេដ</h2>
					<p>GreatX Center</p>
				</ul> -->
				
				<!-- </nav> -->
			</div>
		</header>

	</main>
</body>
</html>
